---
title: "Run Docker as Non Root"
date: 2018-08-23T14:30:18+01:00
tags: [ "linux", "containers" ]
---

![Centered image](/images/docker.png#align-center)

# Want to run docker as non root user?

This has a variaty of use cases, mine specifically was that I was using docker to download some files around and wanted another service with different permissions to access those files. Another use case is when building software in docker, you need your Jenkins build server to be able to access the files in the next step of the build. 

First you need to add a docker group and add your user to it

    sudo groupadd docker
    sudo usermod -aG docker $USER

The simply log in as your user and apply as below:
    
    /bin/docker run -d \                             # Run docker in daemon mode
          -u $( id -u $USER ):$( id -g $USER ) \     # Run 
          --name worker1 \
          worker

