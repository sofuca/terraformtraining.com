---
title: "Useful Linux Commands"
date: 2019-04-03T09:12:01+01:00
draft: false
tags: [ "linux" ]
---
### Add an  extra line to a number of files

    echo "LINE TO ADD"  | tee -a FILE*

### Run a command recursivly on an entire directory

    find . -type f -print0 | xargs -0 dos2unix

### Simple for loop 

This is useful if you want to run the same command on a list of IP addresses
Here we have a file called `ip-list`

    192.168.1.1
    192.168.1.2
    192.168.1.3
    192.168.1.4
    192.168.1.5

And here we have the script that will loop through the list if IP addresses and perform a specific command

    #!/bin/bash
    machine='ip-list'
    machines=`cat $machine`
    echo Starting
    for line in ${machines}; do
        echo ${line}
        scp -i privatekey file root@${line}:/folder/you/want/
    done

### Backing up and restoring PGSQL

Backup to tar file

    pg_dump -F t ${dbname} > /data/backups/DBdump-${dbname}.tar

And restore

Create new DB

    postgres=> create database marktest;
    CREATE DATABASE
    postgres=> grant all privileges on database marktest to postgres;
    GRANT

And the restore command

    pg_restore -c -i -h database.cr9qvdw7ldmz.ap-south-1.rds.amazonaws.com -U postgres -d test -v "DBdump-${dbname}.tar" -W

### Renaming all files in a folder

    for f in *; do mv "$f" "${f/Huge/Monstrous}"; done

