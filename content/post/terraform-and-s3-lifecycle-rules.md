---
title: "Terraform and S3 Lifecycle Rules"
date: 2018-09-19T10:37:25+01:00
draft: false
tags: [ "terraform" ]
---

Object Lifecycle Management in S3 is used to manage your objects so that they are stored cost effectively throughout their lifecycle. Simply put, this means that you can save money if you move your S3 files onto cheaper storage and then eventually delete the files as they age or are accessed less frequently.

There are two types of actions:

+ Transition actions
  - Define when objects transition to another storage class.
  - Storage classes are STANDARD, STANDARD_IA, ONEZONE_IA and GLACIER. 
  - More information can be found [here.](https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html "AWS S3")
+ Expiration actions
  - Define when objects expire (are deleted).

So lets get this all up and running with Terraform, first lets create a bucket and a few folders within the bucket.

    resource "aws_s3_bucket" "lifecycle-testing" {
      bucket = "lifecycle-testing12345"
      acl    = "private"
    }
    
    resource "aws_s3_bucket_object" "lifecycle-testing-prod" {
      bucket = "${aws_s3_bucket.lifecycle-testing.id}"
      acl    = "private"
      key    = "prod/"
      source = "/dev/null"
    }
    
    resource "aws_s3_bucket_object" "lifecycle-testing-prod-archive" {
      bucket = "${aws_s3_bucket.lifecycle-testing.id}"
      acl    = "private"
      key    = "archive/"
      source = "/dev/null"
    }

We then run `terraform init` to download the correct provider then `terraform plan` to check our configuration does not contain any errors.Once we are happy then we can run `terraform apply`. This will create our bucket and two folders within the bucket.

Okay, now lets go over to our bucket and see how that looks.

![Centered image](/images/S3.PNG)

Great - the bucket has been created with the two folders. Now we'll apply a lifecycle rule to just the archive folder.

    resource "aws_s3_bucket" "lifecycle-testing" {
      bucket = "lifecycle-testing12345"
      acl    = "private"
    
      lifecycle_rule {
        id      = "archive"
        enabled = true
    
        prefix = "archive/"
    
        tags {
          "rule" = "archive"
        }
    
        transition {
          days          = 30
          storage_class = "ONEZONE_IA"
        }
    
        transition {
          days          = 90
          storage_class = "GLACIER"
        }
    
        expiration {
          days = 120
        }
      }
    }

This policy will move all data in the /archive folder into ONEZONE_IA stogage class after 30 days, GLACIER after 90 days and delete the files completely after 120 days.

So now we can run `terraform apply` and then take a look at the policy in AWS. 

Lets click on the management tab at the top of the page.
![Centered image](/images/s31.PNG)

We can now see our policy, if we highight the policy then click edit we can take a more detailed look.
![Centered image](/images/s32.PNG)

Great, now we can see our transition policy.
![Centered image](/images/s33.PNG)

And on the next page our expiration policy.
![Centered image](/images/s34.PNG)

Hopefully this will save you some money and help you better understand S3 lifecycle rules. Please remember to fully test your settings before applying to any production environments. Have fun!
