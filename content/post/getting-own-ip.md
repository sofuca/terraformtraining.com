---
title: "Getting Your Own Public IP With Terraform "
date: 2018-09-24T08:27:13+01:00
draft: false
tags: [ "terraform" ]
---

This is a very handy trick to get your own public IP within Terrafrom. Very useful if your public IP changes a lot and you want to use it with a security group.

    provider "aws" {
      region = "eu-west-1"
    }
    
    data "external" "myipaddr" {
      program = ["bash", "-c", "curl -s 'https://ipinfo.io/json'"]
    }
    
    output "my_public_ip" {
      value = "${data.external.myipaddr.result.ip}"
    }

The simply run `terrafrom init` to download your provider and then `terrafrom apply` to run.

The output is below, I have changed the IP for security reasons.


    data.external.myipaddr: Refreshing state...
    
    Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
    
    Outputs:
    
    my_public_ip = 8.8.8.8
