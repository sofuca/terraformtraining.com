---
title: "How To Install Terraform"
date: 2017-07-23T18:43:54+01:00
tags: [ "terraform", "linux" ]
---
![Centered image](/images/terra.png#align-center)

# Install Terraform

[Terraform](https://www.terraform.io/downloads.html "Click here to download Terraform") is distributed as a binary package for all supported platforms and architectures. Older versions are avaliable [here](https://releases.hashicorp.com/terraform/ "Click to download older versions").

Installing Terraform is a relatively trivial affair. Simply head over to [Terraform](https://www.terraform.io/downloads.html "Click here to download Terraform") select your operating system and download the corresponding zip file. Once downloaded extract the file to a convienient location on your computer and add to your path. 

Terraform version history is located [here](https://github.com/hashicorp/terraform/blob/v0.11.13/CHANGELOG.md "Version history").

### Install Terraform on Linux

#### Method 1

The easiest way to install Terraform on Linux is download the file using wget then unzip the file and then move to `/usr/local/bin/`.

Download [Terraform](https://www.terraform.io/downloads.html "Click here to download Terraform") for Linux.

    wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip

You may first need to install unzip.

CentOS.
    
    
    sudo yum install unzip -y

Ubuntu.

    sudo apt-get install unzip -y

The unzip Terraform and move to path.

    unzip terraform_0.11.8_linux_amd64.zip
    sudo mv terraform /usr/local/bin/

#### Method 2

Or you can download and extract to a location not in your path and add it. 
Here I download [Terraform](https://www.terraform.io/downloads.html "Click here to download Terraform") to my Downloads folder, extracted the zip file, moved it to `/home/terraform/Documents/terraform` and then updated the PATH environment variable pointing it to Terraform.

Unzip Terraform.

    unzip terraform_0.9.11_linux_amd64.zip
    Archive:  terraform_0.9.11_linux_amd64.zip
      inflating: terraform

Move to chosen location.

    mv terraform ../Documents/terraform/

And add to path.

    export PATH=$PATH:/home/terraform/Documents/terraform:$PATH

Verify Terraform is installed and working correctly.
    
    Usage: terraform [-version] [-help] <command> [args]
    
    The available commands for execution are listed below.
    The most common, useful commands are shown first, followed by
    less common or more advanced commands. If you're just getting
    started with Terraform, stick with the common commands. For the
    other commands, please read the help and docs before usage.
    
    Common commands:
        apply              Builds or changes infrastructure
        console            Interactive console for Terraform interpolations
        destroy            Destroy Terraform-managed infrastructure
        env                Workspace management
        fmt                Rewrites config files to canonical format
        get                Download and install modules for the configuration
        graph              Create a visual graph of Terraform resources
        import             Import existing infrastructure into Terraform
        init               Initialize a Terraform working directory
        output             Read an output from a state file
        plan               Generate and show an execution plan
        providers          Prints a tree of the providers used in the configuration
        push               Upload this Terraform module to Atlas to run
        refresh            Update local state file against real resources
        show               Inspect Terraform state or plan
        taint              Manually mark a resource for recreation
        untaint            Manually unmark a resource as tainted
        validate           Validates the Terraform files
        version            Prints the Terraform version
        workspace          Workspace management
    
    All other commands:
        debug              Debug output management (experimental)
        force-unlock       Manually unlock the terraform state
        state              Advanced state management


Success!

### Install Terraform on Mac OS

The easiest way to install [Terraform](https://www.terraform.io/downloads.html "Click here to download Terraform") on Mac is to use Homebrew.

    brew install terraform

### Install Terraform on Windows 

The easiest way to install [Terraform](https://www.terraform.io/downloads.html "Click here to download Terraform") on Windows is to download the zip file, extract the terrafrom.exe file and move it to `c:\windows\system32`

If you want to store it somewhere else use the instructions below.

Here I created a folder called terraform in my C:\ then extracted the zip file. After that I updated the PATH environment variable pointing it to Terraform.

    cd \
    mkdir terraform

I then used [7-zip](http://www.7-zip.org/download.html "Click here to download 7-zip") to extract the terraform.exe to c:\terraform and add `c:\terraform` to my path.

    set PATH=%PATH%;C:\terraform

Then to test

    C:\terraform>terraform.exe -version
    Terraform v0.9.11

And the next post I'll show you how to sign up to [Amazon Web Services](https://aws.amazon.com/ "Click here to sign up to AWS") and set up some credentials!







