+++
title = "About Me"
date = 2017-07-03T02:13:50Z
author = "Mark Burke"
description = "Mark Burke, DevOps Engineer living in Notttingham"
+++

## About
I've been working in IT for over 10 years and have extensive experience in a range of positions.

I started with user support, then moved into managing infrastructure for large organisations. 

I’m currently a Sysadmin/DevOps Engineer for [ONYX InSight](https://onyxinsight.com/) based in Nottingham, England.

I spend most of my time working with [Amazon Web Services](https://aws.amazon.com/), [CentOS](https://www.centos.org/),[Terraform](https://www.terraform.io/) and [Docker](https://www.docker.com/).
