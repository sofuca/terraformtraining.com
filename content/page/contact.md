+++
title = "Contact"
date = 2015-04-03T02:13:50Z
author = "My Name"
description = "How to contact me."
+++

## Contact

Hello, you can contact me via [LinkedIn](https://uk.linkedin.com/in/mark-burke-59011892) or click the mail icon at the top of the page.
