#!/bin/bash
hugo --theme=ghostwriter
cd public
aws s3 sync . s3://letslearndevops.com --delete --acl public-read
echo "site updated"
