
# Home of terraformtraining.com

### Create new post

Make sure you're in the root folder and enter:

`hugo new post/example.md`

### Edit

Edit by opening example.md and adding content

### Build new post 

`hugo server --buildDrafts`

### Adding Images

Grab an image from somewhere, name it example.jpg, and place it in the `terraformtraining/static/images` directory.

also edit example.md to add image 

`+++
date = "2017-02-19T21:09:05-06:00"
draft = true
title = "example"
image = "example.jpg"
+++`


### Make posts public

`hugo undraft content/post/example.md`

### Make live 

Run ./deploy.sh