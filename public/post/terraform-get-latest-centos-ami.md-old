---
title: "Using Terraform Data Sources To Get The Latest AMI"
date: 2018-08-23T19:18:16+01:00
tags: [ "aws", "terraform" ]
---

![Centered image](/images/Amazon-Machine-Image.png#align-center)

# Querying the AWS API for the latest Amazon Machine Images

Is a great time saver, it stops you from having to hard code any AMI ids within your code. This solution will also enable you to get the latest AMI in the regon you're working in without changing anything. Good stuff! 

### To use Terraform to get the latest Unubtu AMI

#### First our provider block

    provider "aws" {
        region = "eu-west-1" # Ireland region, change as you wish
        access_key = "1234567890"
        secret_key = "1234567890"
    }

#### Now lets define our instance

    resource "aws_instance" "my_first_instance" {
        ami           = "${data.aws_ami.latest-ubuntu.id}"
        instance_type = "t2.micro"
    }

#### And now lets query the AWS API for the latest Ubuntu AMI

    data "aws_ami" "latest-ubuntu" {
        most_recent = true

        filter {
            name   = "name"
            values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
        }

        filter {
            name   = "virtualization-type"
            values = ["hvm"]
        }

        owners = ["099720109477"] # Canonical
    }

Now all you have to do is run `terraform init` to download the AWS provider, then `terraform plan` to check your code and finally `terraform apply` to make it live.

#### If you are intrested in CentOS7 or use AWS ECS AMIs the code for them is just below

Latest ECS AMI

    data "aws_ami" "latest_ecs" {
    most_recent = true

    filter {
        name   = "name"
        values = ["*amazon-ecs-optimized"]
    }

    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["591542846629"] # AWS
    }

Latest CentOS7 AMI

    data "aws_ami" "centos" {
    owners      = ["679593333241"]
    most_recent = true

    filter {
        name   = "name"
        values = ["CentOS Linux 7 x86_64 HVM EBS *"]
    }

    filter {
        name   = "architecture"
        values = ["x86_64"]
    }

    filter {
        name   = "root-device-type"
        values = ["ebs"]
    }
    }

Hopefully this will save you some time! 